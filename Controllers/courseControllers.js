const Course = require("../Models/Course");

//Create a new course
/*
Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new User to the database.

*/

module.exports.addCourse = (reqBody) =>{

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
		
	})

	return newCourse.save().then((course, error) =>{
		if(error){
			return false}
		else
		{

			return true
		}
	})
}


// Retrirve all courses

/*
	Steps:
		1. Retrieve all courses from the database
*/
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result);
}
// Retrieve all ACTIVE courses
/*
	Steps:
	1. Retrieve sll the courses from the database with the property of "isActive" to true.
*/
							// parameter
module.exports.getAllActive = () =>{
	return Course.find({isActive:true}).then(result => result);
}

// Retrieving a specific course
/*
	Steps:
	1. Retrirve the course that matches the courseID provoded from the URL.
*/
module.exports.getCourse = (courseId) =>{
	return Course.findById(courseId).then(result => result);
}

// Update a course
/*
	Steps:
	1. Create a variable for the updated course which will contain the information retrirved from the request body.
	2. Find and update the courseusing the find by ID (courseId) retrieved from the request params/URL property and the variable "updatedCourse" containing the information from the request body.
*/

module.exports.updateCourse = (courseId, reqBody) =>{
	// specify the fields or properties to be updated
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}

	// Syntax: findbyIdAndUpdate(documentId, updatesToBeApplied)

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

// archive a course

/*
	Mini Activitty:
	1. create a archiveCourse function that will change the status of an active course to inactive using the "isActive" property.
	2. Return true if the is active course is set to inactive and false if encountered an error.
	2. Once done, send a screenshot of your postman in the batch hangouts.
*/

module.exports.archiveCourse = (courseId, reqBody) =>{
	let updateActiveField = {
		isActive: false
	}

return Course.findByIdAndUpdate(courseId, updateActiveField).then((isActive, error) =>{
	if(error){
		return false;

	}
	else{
		return true;
	}
})

}