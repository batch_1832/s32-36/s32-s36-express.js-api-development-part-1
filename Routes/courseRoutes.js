const express = require("express");
const router = express.Router();
const courseControllers = require("../Controllers/courseControllers");
const auth = require("../auth");


router.post("/", auth.verify, (req, res) =>{

const userDate = auth.decode(req.headers.authorization);
// If rhe user is admin, proceed with the course creation
if(userData.isAdmin){

	courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));
}
// If user is not admin, send a response "You dont have permission to access this page!".
else{
	res.send("You dont have permission to access this page!")
}

})

// Route for retrirving all the courses
router.get("/all", auth.verify, (req, res) =>{
	courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrirving all active courses
router.get("/", (req, res) =>{
	courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
router.get("/:courseId", (req, res) =>{
	console.log(req.params.courseId);

	courseControllers.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{
									// search key		// update
	courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})

// route to archive a course
router.patch("/:courseId/archive", auth.verify, (req, res) =>{
	courseControllers.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;