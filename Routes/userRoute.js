const express = require("express");
const router = express.Router();

const userControllers = require("../Controllers/userControllers");
const auth = require("../auth");

// const auth = require("../auth")

// Router for checking if the email exists
router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Router for the user registration
router.post("/register",(req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for user login
router.post("/login", (req, res) =>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for detailsRoute

router.post("/details", (req, res) =>{
	userControllers.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})


// Route to enroll a course
router.post("/enroll", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)

	let data = {
		// UserId will be retrirvrd from the request header
		userId: userData.id,
		// courseId will be retrirved from the request body
		courseId: req.body.courseId
	}
	if(userData.isAdmin){
		res.send("You are not allowed to access this page!")
	}
	else{
		return false;
	}

	userControllers.enroll(data).then(resultFromController => res.send(resultFromController));
})


module.exports = router;


