// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoute")

const courseRoutes = require("./Routes/courseRoutes")
// Create server
const app = express();
const port = 4000;

// Connect to  our mongoDB database
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.bcygx.mongodb.net/course-booking-app?retryWrites=true&w=majority", {
	useNewURLParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure of mongo db
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


// Middlewares
// allow all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes for our API
app.use("/users", userRoutes);

app.use("/courses", courseRoutes);

// Listening to port
// This syntax will allow flexibility when using the application locally or as a hosted application.
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`);
})